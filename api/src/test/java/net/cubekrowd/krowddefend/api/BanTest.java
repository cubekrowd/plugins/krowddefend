package net.cubekrowd.krowddefend.api;

import java.io.*;
import java.util.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.command.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class BanTest {

    private static final File TEST_DIR = new File("test_data_ban");
    private static final File TEST_FILE = new File("data.yml");

    @BeforeEach
    void setupStorage() {
        TEST_DIR.mkdir();
        if(EventStorageAPI.getStorage() == null) {
            EventStorageAPI.registerInit(new StorageYAML(new EventStoragePlugin() {
                public List<String> getAuthors() {
                    return null;
                }
                public String getVersion() {
                    return null;
                }
                public void callEvent(ESAPIEvent event, Object... data) {
                }
                public boolean isDebug() {
                    return false;
                }
                public List<Messagable> getDebuggers() {
                    return new ArrayList<>();
                }
            }, TEST_DIR));
        }
        EventStorageAPI.getStorage().open();

        if(KrowdDefend.getAPI() == null) {
            KrowdDefend.setInstance(new KrowdDefendAPI(new KrowdDefendPlugin() {}));
        }
    }

    @AfterEach
    void testDown() {
        EventStorageAPI.getStorage().close();
        TEST_FILE.delete();
        TEST_DIR.delete();
    }


    @Test
    void testBanPlayer() {
        UUID u1 = UUID.fromString("7e1b2286-e794-46fd-ba8b-f806dc7062e1");

        KrowdDefend.getAPI().banPlayer(u1, "", KrowdDefendAPI.CONSOLE);
        assertTrue(KrowdDefend.getAPI().isBanned(u1));
    }

    @Test
    void testUnbanPlayer() {
        UUID u1 = UUID.fromString("7e1b2286-e794-46fd-ba8b-f806dc7062e1");

        KrowdDefend.getAPI().banPlayer(u1, "", KrowdDefendAPI.CONSOLE);
        assertTrue(KrowdDefend.getAPI().isBanned(u1));
        KrowdDefend.getAPI().unbanPlayer(u1, KrowdDefendAPI.CONSOLE);
        assertFalse(KrowdDefend.getAPI().isBanned(u1));
    }
}