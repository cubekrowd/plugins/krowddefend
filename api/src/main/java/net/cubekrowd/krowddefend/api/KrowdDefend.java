package net.cubekrowd.krowddefend.api;

import lombok.*;

public final class KrowdDefend {

    private KrowdDefend() {}

    @Getter
    private static KrowdDefendAPI API = null;

    public static void setInstance(KrowdDefendAPI api) {
        if(API != null) {
            throw new UnsupportedOperationException("Cannot redefine singleton Server");
        }
        API = api;
    }

}