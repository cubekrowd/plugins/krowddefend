package net.cubekrowd.krowddefend.api;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import net.cubekrowd.eventstorageapi.api.*;
import lombok.*;

@RequiredArgsConstructor
public final class KrowdDefendAPI {

    @NonNull @Getter private final KrowdDefendPlugin plugin;

    public static final UUID CONSOLE = new UUID(0, 0);

    public boolean isBanned(@NonNull UUID target) {
        long time = System.currentTimeMillis();
        AtomicBoolean result = new AtomicBoolean(false);
        EventStorageAPI.getStorage().getEntryStream("krowddefend", Arrays.asList("ban", "unban"), Map.of("p", target.toString())).sorted().forEach(ee -> {
            if(ee.getType().equals("unban")) {
                result.set(false);
                return;
            }
            if(ee.getType().equals("ban")) {
                long time2 = Long.parseLong(ee.getData().get("u"));
                if(time2 == -1 || time2 > time) {
                    result.set(true);
                }
            }
        });
        return result.get();
    }

    public void banPlayer(@NonNull UUID target, @NonNull String reason, @NonNull UUID by) {
        banPlayer(target, reason, -1, by);
    }

    public void banPlayer(@NonNull UUID target, @NonNull String reason, long until, @NonNull UUID by) {
        EventStorageAPI.getStorage().addEntry(new EventEntry("krowddefend", "ban", Map.of("p", target.toString(), "r", reason, "u", until + "", "b", by.toString())));
    }

    public void unbanPlayer(@NonNull UUID target, @NonNull UUID by) {
        EventStorageAPI.getStorage().addEntry(new EventEntry("krowddefend", "unban", Map.of("p", target.toString(), "b", by.toString())));
    }

}
