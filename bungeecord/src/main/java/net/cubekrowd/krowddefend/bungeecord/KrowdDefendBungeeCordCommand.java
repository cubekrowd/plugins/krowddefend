package net.cubekrowd.krowddefend.bungeecord;

import java.util.*;
import java.io.*;
import net.cubekrowd.krowddefend.api.*;
import net.md_5.bungee.config.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.plugin.*;

public class KrowdDefendBungeeCordCommand extends Command {

    private final KrowdDefendBungeeCordPlugin plugin;

    public KrowdDefendBungeeCordCommand(KrowdDefendBungeeCordPlugin plugin) {
        super("krowddefend", null, "kd");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] argsa) {
        List<String> args = Arrays.asList(argsa);

        if (args.size() == 1 && args.get(0).equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("krowddefend.admin")) {
                sender.sendMessage(ChatColor.RED +
                                   "You do not have permission to execute this command!");
                return;
            }

            try {
                plugin.setConfig(plugin.getUtils().loadConfig("config.yml"));
            } catch (Exception e) {
                throw new RuntimeException("Unable to read config and data file", e);
            }
            sender.sendMessage(plugin.prefix + ChatColor.DARK_GREEN +
                               "Reloaded configuration!");
            return;
        }

        if (args.size() >= 3 && args.get(0).equalsIgnoreCase("ban")) {
            if (!sender.hasPermission("krowddefend.command.ban")) {
                sender.sendMessage(ChatColor.RED +
                                   "You do not have permission to execute this command!");
                return;
            }

            UUID target = plugin.getUtils().getUuidFromName(args.get(1), true);
            String reason = String.join(" ", args.subList(2, args.size()));

            KrowdDefend.getAPI().banPlayer(target, reason, plugin.getUtils().getSenderUniqueId(sender));
            return;
        }

        if (args.size() == 2 && args.get(0).equalsIgnoreCase("unban")) {
            if (!sender.hasPermission("krowddefend.command.unban")) {
                sender.sendMessage(ChatColor.RED +
                                   "You do not have permission to execute this command!");
                return;
            }

            UUID target = plugin.getUtils().getUuidFromName(args.get(1), true);

            KrowdDefend.getAPI().unbanPlayer(target, plugin.getUtils().getSenderUniqueId(sender));
            return;
        }

        if (args.size() == 2 && args.get(0).equalsIgnoreCase("info")) {
            if (!sender.hasPermission("krowddefend.command.info")) {
                sender.sendMessage(ChatColor.RED +
                                   "You do not have permission to execute this command!");
                return;
            }

            UUID target = plugin.getUtils().getUuidFromName(args.get(1), true);

            boolean b = KrowdDefend.getAPI().isBanned(target);
            sender.sendMessage("Player ban status: " + b);
            return;
        }

        if(args.size() != 0 && !args.get(0).equalsIgnoreCase("help")) {
            sender.sendMessage(plugin.prefix + ChatColor.GREEN + "Invalid/unknown command!");
            return;
        }

        sender.sendMessage(plugin.prefix + ChatColor.GREEN + "Running version v" +
                           plugin.getDescription().getVersion());
        boolean any = false;
        if (sender.hasPermission("krowddefend.admin")) {
            sender.sendMessage(plugin.prefix + ChatColor.GOLD + "/kd reload");
            any = true;
        }
        if (sender.hasPermission("krowddefend.command.ban")) {
            sender.sendMessage(plugin.prefix + ChatColor.GOLD + "/kd ban " + ChatColor.GRAY + "<player> [reason]");
            any = true;
        }
        if (sender.hasPermission("krowddefend.command.unban")) {
            sender.sendMessage(plugin.prefix + ChatColor.GOLD + "/kd unban " + ChatColor.GRAY + "<player>");
            any = true;
        }
        if (sender.hasPermission("krowddefend.command.info")) {
            sender.sendMessage(plugin.prefix + ChatColor.GOLD + "/kd info " + ChatColor.GRAY + "<player>");
            any = true;
        }
        if(any) {
            sender.sendMessage(plugin.prefix + ChatColor.GOLD + "/kd help");
        }

    }
}
