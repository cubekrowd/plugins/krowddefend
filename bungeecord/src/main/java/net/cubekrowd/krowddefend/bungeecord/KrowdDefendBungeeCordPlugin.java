package net.cubekrowd.krowddefend.bungeecord;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import lombok.*;
import net.cubekrowd.krowddefend.api.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.pluginutils.PluginUtilsBungee;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;

public class KrowdDefendBungeeCordPlugin extends Plugin implements KrowdDefendPlugin {

    @Getter private PluginUtilsBungee utils;
    @Getter @Setter private Configuration config;

    public final String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.RED +
                                 "Krowd" + ChatColor.DARK_GREEN + "Defend" + ChatColor.DARK_GRAY + "] ";

    @Override @SneakyThrows
    public void onEnable() {
        utils = new PluginUtilsBungee(this);

        config = utils.loadConfig("config.yml");

        if (config.getInt("version") != 1) {
            getLogger().severe(ChatColor.RED +
                               "WRONG CONFIGURATION VERSION. PLEASE BACKUP AND DELETE THE CONFIG.YML. The latest version will be generated automatically during next restart if you have moved the old one. Disabling...");
            onDisable();
            return;
        }

        KrowdDefend.setInstance(new KrowdDefendAPI(this));

        getProxy().getPluginManager().registerCommand(this, new KrowdDefendBungeeCordCommand(this));
    }

    @Override
    public void onDisable() {
        utils = null;
    }

}
