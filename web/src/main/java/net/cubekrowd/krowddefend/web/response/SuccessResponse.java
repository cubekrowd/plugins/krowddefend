package net.cubekrowd.krowddefend.web.response;

import java.util.*;

public class SuccessResponse extends DataResponse {

    public SuccessResponse(Map<String, Object> data) {
        super(ResponseStatus.SUCCESS, data, null);
    }

}