package net.cubekrowd.krowddefend.web.response;

public class ErrorResponse extends DataResponse {

    public ErrorResponse(String message) {
        super(ResponseStatus.ERROR, null, message);
    }

}