package net.cubekrowd.krowddefend.web;

import java.util.*;
import java.io.*;
import lombok.*;
import net.cubekrowd.krowddefend.api.*;
import net.cubekrowd.krowddefend.web.response.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import static spark.Spark.*;

public class KrowdDefendWeb {

    public static void main(String[] args) {
        new KrowdDefendWeb();
    }
    private static final File TEST_DIR = new File("data");
    private static final File TEST_FILE = new File("data.yml");

    public KrowdDefendWeb() {
        // Setup DEV ESAPI, move to mongo later
        TEST_DIR.mkdir();
        EventStorageAPI.registerInit(new StorageYAML(new EmptyEventStoragePlugin(), TEST_DIR));
        EventStorageAPI.getStorage().open();

        // Init KrowdDefendAPI
        KrowdDefend.setInstance(new KrowdDefendAPI(new KrowdDefendPlugin() {}));

        // Setup routes
        get("/ping", (req, res) -> new SuccessResponse(Map.of("pong", System.currentTimeMillis())).parse(res));
        get("/v1/user/:uuid", (req, res) -> KrowdDefend.getAPI().isBanned(UUID.fromString(req.params("uuid"))) + "");
    }

}